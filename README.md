# OpenWRT on Meraki MX65

Documentation and files used during the process of installing OpenWRT on the Cisco Meraki MX65

## Getting started

Read the documentation located in the `docs/` folder to get started.

## Usage
The files contained in this repo are solely intended for use when working with MX65 hardware in an Open Source context.

## Support
This project offers no implied or explicitly offered support for your hardware or the results of any actions taken after accessing this repository's content by any means.

## Roadmap
No roadmap at the moment. This repo is for development and testing efforts on an adhoc basis.

## Contributing
- Message winternull on the OpenWRT forums if you'd like to contribute in some manner.
- Potential contributors must have existing experience with the tools and processes required by the project.

## Authors and acknowledgment
See documentation for additional info.

## License
This project follows the licensing model used by OpenWRT and any GPL-related sources from Meraki that may be involved with image builds.

## Project status
- 2022 July: project repo created
